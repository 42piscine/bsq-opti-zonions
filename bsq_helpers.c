/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq_helpers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/28 08:23:56 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/29 15:46:27 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"

int	ft_strlen(unsigned char *str)
{
	int	cnt;

	cnt = 0;
	while (*str++)
		cnt++;
	return (cnt);
}

int	ft_strslen(unsigned char **strs, int size)
{
	int	cnt;

	cnt = 0;
	while (size-- > 0)
		cnt += ft_strlen(*strs++);
	strs += cnt;
	return (cnt);
}

unsigned char	*ft_strdog(unsigned char *s1, unsigned char *s2)
{
	int				len;
	int				i;
	unsigned char	*str;
	unsigned char	*rep;

	len = ft_strlen(s1) + ft_strlen(s2);
	str = (unsigned char *)malloc(len * sizeof(char));
	rep = str;
	i = -1;
	while (*s1)
	{
		*str = *s1;
		str++;
		s1++;
	}
	while (*s2)
	{
		*str = *s2;
		str++;
		s2++;
	}
	*str = '\0';
	return (rep);
}
