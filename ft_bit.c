/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yalthaus <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 12:35:48 by yalthaus          #+#    #+#             */
/*   Updated: 2021/07/29 21:34:53 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <stdio.h>

unsigned char	*ft_stob(unsigned char *str, t_map *map, int len)
{
	int				i;
	int				j;
	unsigned char	*bytes;

	bytes = (unsigned char *)malloc(len / 8 + 2);
	if (!bytes)
		return (NULL);
	i = 0;
	j = 0;
	printf("\n");
	while (j < len)
	{
		if (*(str + j) == map->obs || *(str + j) == map->empty)
		{
			if (i % 8 == 0)
				*(bytes + (i / 8)) = 0;
			printf("%c[%d]\t", *(str + j), *(str + j));
			if (*(str + j) == map->obs)
			{
				printf("Calculating an obstacle byte\t");
				*(bytes + (i / 8)) |= (1 << (7 - (i % 8)));
			}
			printf("byte : %X\n", bytes[i / 8]);
			i++;
		}
		j++;
	}
	*(bytes + len / 8 + 1) = '\0';
	i = 0;
	return (bytes);
}

bool	ft_bytesmasks(unsigned char *bytes, unsigned char *masks,
	   	unsigned int len, t_map *map)
{
	unsigned int	i;
	unsigned int	l;

	l = ft_strlen(bytes) / 8 + 1;
	i = -1;
	while (++i < len)
	{
		if ((*(bytes + i) | *(masks + i)) < (*bytes + (map->w * i)))
			return (0);
		i++;
	}
	return (1);
}

bool	ft_issqr(t_list *list, unsigned char *mask, t_map *map,
	   	unsigned int len)
{
	unsigned int		i;
	unsigned int		j;
	unsigned int		k;
	unsigned char		*bytes;

	bytes = list->bytes;
	i = -1;
	j = 0;
	k = 0;
	while (j < ((unsigned int)map->w * len))
	{
		if (k >= list->len && list->next != NULL)
		{
			list = list->next;
			bytes = ft_strdog(bytes, list->bytes);
			k = 0;
		}
		if ((bytes + j) == map->ptr)
		{
			return (ft_bytesmasks(bytes, mask, len, map));
		}
		k++;
		j++;
	}
	return (1);
}

unsigned char	*ft_mask(unsigned char *bytes, unsigned int i, unsigned int len)
{
	unsigned int				j;
	unsigned int				k;
	unsigned int				start;
	unsigned char				*mask;

	if (i < len)
		start = 0;
	else
		start = (i - len) / 8;
	j = 0;
	mask = (unsigned char *)malloc(ft_strlen(bytes));
	while (*(bytes + j))
	{
		*(mask + j) |= 255;
		j++;
	}
	j = 0;
	k = 0;
	while (j < len)
	{
		*(mask + k + start) ^= (1 << (start + j) % 8);
		if ((start + j) % 8 == 0)
			k++;
		j++;
	}
	return (mask);
}
