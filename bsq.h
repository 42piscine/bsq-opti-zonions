/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BSQ.h	                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yalthaus <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/26 14:11:05 by yalthaus          #+#    #+#             */
/*   Updated: 2021/07/29 21:34:04 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BSQ_H
# define BSQ_H

# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>

# define BUF_SIZE 32

typedef struct s_cntr
{
	unsigned int	bfull;
	unsigned short	bpos;
	unsigned char	nbcmeta;
	unsigned int	nblines;
}	t_cntr;

typedef struct s_list
{
	unsigned char	*bytes;
	unsigned int	len;
	struct s_list	*next;	
}	t_list;

typedef struct s_square
{
	unsigned char	*ptr;
	unsigned int	len;
	unsigned int	i;
}	t_square;

typedef struct s_map
{
	t_list			list;
	t_square		bigsqr;
	unsigned int	w;
	unsigned int	h;
	char			empty;
	char			obs;
	char			full;
	bool			is_valid;
	unsigned char	*ptr;
}	t_map;

t_list			*ft_add_list(t_list *list, int len, unsigned char *buf,
					t_map map);
unsigned char	*ft_mask(unsigned char *bytes, unsigned int i,
					unsigned int len);
bool			ft_issqr(t_list *list, unsigned char *mask, t_map *map,
					unsigned int len);
void			ft_get_maxsquare(t_list *list, t_map *map);
t_map			ft_get_map(int fd, t_list **blist);
unsigned char	*ft_stob(unsigned char *str, t_map *map, int len);
//void			add_list(t_list *list, int len, unsigned char *buf, t_map map);
unsigned char	*ft_strdog(unsigned char *s1, unsigned char *s2);
int				ft_strlen(unsigned char *str);
void			print_list(t_list *list);
unsigned int	ft_linelen(t_cntr cntr, unsigned int lwidth);
bool			ft_get_map_meta(unsigned char *meta, int len, t_map *map);
bool			ft_parse_map(t_cntr *cntr,
					unsigned char *buf,
					unsigned short buf_size,
					t_map *map);

#endif /*BSQ_H*/
