/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/28 03:02:30 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/29 16:58:16 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <stdio.h>

bool	ft_all_different(char a, char b, char c)
{
	return (a != b && a != c && b != c);
}

bool	ft_get_map_meta(unsigned char *meta, int len, t_map *map)
{
	int		mult;

	if (len < 4)
	{
		map->is_valid = false;
		return (false);
	}
	map->full = *meta--;
	map->obs = *meta--;
	map->empty = *meta--;
	if (!ft_all_different(map->full, map->obs, map->empty))
	{
		map->is_valid = false;
		return (false);
	}
	len -= 3;
	mult = 1;
	while (len-- > 0)
	{
		if (*meta < '0' || *meta > '9')
			map->is_valid = false;
		if (!map->is_valid)
			return (false);
		map->h += (*meta-- - '0') * mult;
		mult *= 10;
	}
	return (true);
}

bool	ft_check_height(t_cntr *cntr, t_map *map, unsigned char *buf)
{
	if (!map->h)
	{
		cntr->nbcmeta = cntr->bfull * BUF_SIZE + cntr->bpos;
		if (!ft_get_map_meta((buf - 1), cntr->nbcmeta, map))
		{
			printf("Failed at map meta\n");
			map->is_valid = false;
		}
	}
	else
	{
		cntr->nblines++;
	}
	return (map->is_valid);
}

bool	ft_check_width(t_cntr cntr, t_map *map)
{
	if (map->h && !map->w)
	{
		map->w = cntr.bfull * BUF_SIZE + cntr.bpos - cntr.nbcmeta;
	}
	else if (map->h)
	{
		if (cntr.bfull * BUF_SIZE - cntr.nbcmeta + cntr.bpos
			- cntr.nblines * map->w != map->w)
		{
			printf("failed width\n");
			map->is_valid = false;
		}
	}
	return (map->is_valid);
}

bool	ft_parse_map(t_cntr *cntr,
		unsigned char *buf,
		unsigned short buf_size,
		t_map *map)
{
	while (map->is_valid && ++(cntr->bpos) < buf_size && *buf)
	{
		write(1, buf, 1);
		if (*buf == '\n')
		{
			if (!ft_check_width(*cntr, map)
				|| !ft_check_height(cntr, map, buf))
			{
				return (false);
			}
		}
		else if (cntr->nbcmeta >= 4)
		{
			if (*buf != map->empty && *buf != map->obs)
				map->is_valid = false;
		}
		if (cntr->bpos < buf_size)
			buf++;
	}
	return (true);
}
