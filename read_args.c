/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_args.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/26 23:09:26 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/29 21:36:27 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <stdio.h>

void	ft_init_cntr(t_cntr *cntr)
{
	cntr->bfull = -1;
	cntr->nblines = 0;
	cntr->nbcmeta = 0;
}

void	ft_init_map(t_map *map)
{
	map->h = 0;
	map->w = 0;
	map->is_valid = true;
	map->bigsqr.len = 0;
	map->bigsqr.ptr = NULL;
}

int	ft_new_buffer(int fd, void *buf, unsigned char **b, t_map *map)
{
	int	buf_size;

	buf_size = read(fd, buf, BUF_SIZE);
	if (buf_size < 0)
	{
		map->is_valid = false;
		return (-1);
	}
	*b = &(buf[0]);
	return (buf_size);
}

t_map	ft_get_map(int fd, t_list **blist)
{
	t_map			map;
	unsigned short	buf_size;
	unsigned char	buf[BUF_SIZE];
	unsigned char	*b;
	t_cntr			cntr;

	ft_init_map(&map);
	ft_init_cntr(&cntr);
	buf_size = ft_new_buffer(fd, buf, &b, &map);
	if (buf_size < 0)
		map.is_valid = false;
	while (map.is_valid && buf_size > 0)
	{
		cntr.bfull++;
		cntr.bpos = -1;
		if (!ft_parse_map(&cntr, b, buf_size, &map))
			break ;
		if (cntr.bfull == 0)
		{
			printf("Adding first buffer");
			*blist = ft_add_list(*blist, buf_size - cntr.nbcmeta,
					&(buf[cntr.nbcmeta]), map);
		}
		else
			*blist = ft_add_list(*blist, buf_size, &(buf[0]), map);
		buf_size = ft_new_buffer(fd, buf, &b, &map);
	}
	if (map.is_valid && cntr.nblines != map.h)
		map.is_valid = false;
	close(fd);
	return (map);
}
