/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/28 15:52:52 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/29 21:57:58 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "bsq.h"
#include <stdio.h>

void	ft_puterr(void)
{
	write(2, "map error\n", 10);
}

void	ft_launch_solver(t_map *map, t_list *blist)
{
	if (!map->is_valid)
		ft_puterr();
	else
	{
		printf("\n%p\n", blist);
		ft_get_maxsquare(blist, map);
	}
}

void	ft_init_list(int fd, t_list *blist, t_map *map)
{
	blist = NULL;
	*map = ft_get_map(fd, &blist);
	ft_launch_solver(map, blist);
}

int	main(int argc, char **argv)
{
	int		i;
	int		fd;
	t_map	map;
	t_list	*blist;

	blist = NULL;
	if (argc == 1)
	{
		ft_init_list(0, blist, &map);
	}
	else
	{
		i = 1;
		while (i < argc)
		{
			fd = open(argv[i++], O_RDONLY);
			if (fd < 0)
			{
				ft_puterr();
				continue ;
			}
			ft_init_list(fd, blist, &map);
		}
	}
	return (0);
}
