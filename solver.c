/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yalthaus <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/28 19:16:32 by yalthaus          #+#    #+#             */
/*   Updated: 2021/07/29 22:00:10 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "bsq.h"

void	ft_mxsqrini(unsigned int *i, unsigned int *len, unsigned char **bytes,
	   	t_list *list)
{
	*i = 0;
	*bytes = list->bytes;
	*len = 0;
}

void	ft_mxif(unsigned char **bytes, unsigned int *i,
		unsigned int *len, t_map **map)
{
	if (!(**bytes & (1 << (7 - *i % 8))))
	{
		(*map)->ptr = *bytes + *i;
		(*len)++;
	}
	else
	{
		*len = 0;
	}
}

void	ft_get_maxsquare(t_list *list, t_map *map)
{
	unsigned int	i;
	unsigned int	len;
	unsigned char	*bytes;
	unsigned char	*b;

	ft_mxsqrini(&i, &len, &bytes, list);
	while (*(bytes + (i / 8)))
	{
		if (i % (list->len * 8 + 1) == 0 && i != 0)
		{
			i = 0;
			list = list->next;
			bytes = ft_strdog(bytes, list->bytes);
		}
		b = bytes + i / 8;
		ft_mxif(&b, &i, &len, &map);
		if (len > map->bigsqr.len)
		{
			if (ft_issqr(list, ft_mask(bytes, i, len), map, len))
			{
				map->bigsqr.ptr = map->ptr;
				map->bigsqr.len = len;
				map->bigsqr.i = i % 8;
			}
		}
		i++;
	}
}
