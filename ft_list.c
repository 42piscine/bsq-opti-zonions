/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: yalthaus <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 21:36:35 by yalthaus          #+#    #+#             */
/*   Updated: 2021/07/29 21:35:28 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "bsq.h"
#include <stdio.h>

t_list	*ft_add_list(t_list *list, int len, unsigned char *buf, t_map map)
{
	t_list	*node;
	t_list	*l;

	l = list;
	node = (t_list *)malloc(sizeof(t_list));
	if (node == NULL)
		return (NULL);
	node->bytes = ft_stob(buf, &map, len);
	node->len = len;
	node->next = NULL;
	printf("\n%p\n", list);
	if (list == NULL)
	{
		printf("First node\n");
		return (node);
	}
	else
	{
		printf("Other node\n");
		while (*node->bytes++)
			printf("%X\n", *node->bytes);
		while (list->next != NULL)
			list = list->next;
		list->next = node;
	}
	return (l);
}
